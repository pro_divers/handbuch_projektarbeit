# Handbuch Projektarbeit

Das Handbuch Projektarbeit ist ein Selbstlerntool, das Studierende bei der **selbstständigen Durchführung von Projektarbeiten** unterstützt.

Es wurde im Rahmen des Projekts „Pro_Divers – Neue Wege der individuellen Kompetenzförderung in Projektarbeiten durch hybride Lernarrangements“ entwickelt, das im Bachelorstudiengang Verfahrens-, Energie- und Umwelttechnik (VEU) an der Hochschule Hannover durchgeführt wurde.

Das Handbuch kann von verschiedenen Studiengänge genutzt werden. Mit Hilfe der Anleitung können bei Bedarf aber auch Veränderungen vorgenommen werden.

## Anleitungen

* [Installieren von „Sozi“ und „Inkscape“ (Für Windows)](doc/Anleitung_Sozi_Inkscape_Installieren.pdf)
* [Veränderungen am Puzzle](doc/Anleitung_Veränderungen_am_Puzzle.pdf)

## Arbeitsgruppe Pro Divers

Das Projekt „Pro Divers“ der Hochschule Hannover wurde im Rahmen des niedersächsischen Programms „Innovation Plus“ gefördert.

Konzeption und Inhalte:
* Prof. Dr. Anne Nadolny
* Monika Stöhr

Gestaltung und Technische Umsetzung:
* Prof. Dr. Dennis Allerkamp
* Juliane Kostiuk
* Jannes von Reeken

Kontakt: explore.five@hs-hannover.de

## Lizenzhinweise

* Handbuch Projektarbeit © 2023 Arbeitsgruppe Pro Divers ist lizenziert unter CC BY-NC-SA 4.0. Um eine Kopie dieser Lizenz zu erhalten, besuchen Sie http://creativecommons.org/licenses/by-nc-sa/4.0/.
* Die Schriftart Teko wurde von [Google Fonts](https://fonts.google.com/specimen/Teko/about) unter der [Open Font License](https://github.com/googlefonts/teko/blob/master/OFL.txt) bezogen.
