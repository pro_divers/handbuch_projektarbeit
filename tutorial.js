// Überprüfe, ob die Seite bereits zuvor geöffnet wurde
if (localStorage.getItem('pageOpened')) {
  // Seite wurde bereits geöffnet

  // Überprüfe, ob 20 Sekunden vergangen sind
  var currentTime = new Date().getTime();
  var lastVisitedTime = localStorage.getItem('lastVisitedTime');
  if (lastVisitedTime && (currentTime - lastVisitedTime) < 20000) {
    // Weniger als 20 Sekunden vergangen, lade an anderer Stelle
    window.location.href = '#home';
  } else {
    // 20 Sekunden oder mehr vergangen, lade bestimmte Stelle der Präsentation
    window.location.href = '#tutorial_start';
  }
} else {
  // Seite wird zum ersten Mal geöffnet, lade bestimmte Stelle der Präsentation
  window.location.href = '#tutorial_start';

  // Markiere die Seite als bereits geöffnet
  localStorage.setItem('pageOpened', true);
}

// Speichere die aktuelle Zeit als letzte Besuchszeit
localStorage.setItem('lastVisitedTime', new Date().getTime());

